<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog product configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 12:31
 */

namespace OooAst\Catalog\Model;

use Magento\Catalog\Model\Product as MageProduct;
use Magento\Eav\Api\AttributeSetRepositoryInterface as SetRepositoryAlias;
use Magento\Eav\Api\Data\AttributeSetInterfaceFactory as SetInterfaceAlias;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Zend_Validate_Exception;

/**
 * Catalog product configuration
 *
 * @package OooAst\Catalog\Model
 */
class Product
{
    const ATTR_SET_NAME_KEY = 'attribute_set_name';
    const ENTITY_TYPE_ID_KEY = 'entity_type_id';
    const SORT_ORDER_KEY = 'sort_order';

    /**
     * Product extended attributes
     *
     * @var array
     */
    private $attributes = [
        'english_name' => [
            'group' => 'Part Common',
            'type' => 'varchar',
            'label' => 'English Name',
            'input' => 'input',
            'required' => true,
            'is_unique' => false,
            'sort_order' => 100,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'machine' => [
            'group' => 'Part Common',
            'type' => 'varchar',
            'label' => 'Machine Type',
            'input' => 'select',
            'source' => Attribute\Source\MachineType::class,
            'backend' => Attribute\Backend\MachineType::class,
            'frontend' => Attribute\Frontend\MachineType::class,
            'required' => true,
            'sort_order' => 50,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'machine_unit' => [
            'group' => 'Part Common',
            'type' => 'varchar',
            'label' => 'Machine Unit',
            'input' => 'select',
            'source' => Attribute\Source\MachineUnit::class,
            'backend' => Attribute\Backend\MachineUnit::class,
            'frontend' => Attribute\Frontend\MachineUnit::class,
            'required' => true,
            'sort_order' => 50,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'machine_vendor' => [
            'group' => 'Part Common',
            'type' => 'varchar',
            'label' => 'Machine Vendor',
            'input' => 'multiselect',
            'source' => Attribute\Source\MachineVendor::class,
            'backend' => Attribute\Backend\MachineVendor::class,
            'frontend' => Attribute\Frontend\MachineVendor::class,
            'required' => true,
            'sort_order' => 50,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'machine_model' => [
            'group' => 'Part Common',
            'type' => 'text',
            'label' => 'Machine Model',
            'input' => 'input',
            'frontend' => Attribute\Frontend\MachineModel::class,
            'required' => false,
            'sort_order' => 50,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'part_vendor' => [
            'group' => 'Part Common',
            'type' => 'varchar',
            'label' => 'Part Vendor',
            'input' => 'multiselect',
            'source' => Attribute\Source\PartVendor::class,
            'backend' => Attribute\Backend\PartVendor::class,
            'frontend' => Attribute\Frontend\PartVendor::class,
            'required' => true,
            'sort_order' => 50,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
        'catalog_part_number' => [
            'group' => 'Part Common',
            'type' => 'text',
            'label' => 'Catalog Number',
            'input' => 'input',
            'frontend' => Attribute\Frontend\CatalogPartNumber::class,
            'required' => false,
            'is_unique' => false,
            'sort_order' => 100,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'is_html_allowed_on_front' => true,
            'visible_on_front' => true,
            'attribute_set_id' => 'Part'
        ],
    ];

    /**
     * Product extended attribute sets
     *
     * @var array
     */
    private $attributeSets = [
        'Part' => 'Default',
        'Filter' => 'Part',
        'GET' => 'Part'
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Config
     */
    private $eavConfig;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * @var SetInterfaceAlias
     */
    private $setFactory;
    /**
     * @var SetRepositoryAlias
     */
    private $setRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $criteriaBuilder;
    /**
     * Catalog product entity type ID
     *
     * @var int
     */
    private $productEntityTypeId;

    /**
     * Product constructor.
     *
     * @param LoggerInterface $logger
     * @param Config $eavConfig
     * @param EavSetupFactory $eavSetupFactory
     * @param SetInterfaceAlias $setFactory
     * @param SetRepositoryAlias $setRepository
     * @param SearchCriteriaBuilder $criteriaBuilder
     *
     * @throws LocalizedException
     */
    public function __construct(
        LoggerInterface $logger,
        Config $eavConfig,
        EavSetupFactory $eavSetupFactory,
        SetInterfaceAlias $setFactory,
        SetRepositoryAlias $setRepository,
        SearchCriteriaBuilder $criteriaBuilder
    ) {
        $this->logger = $logger;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->setFactory = $setFactory;
        $this->setRepository = $setRepository;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->productEntityTypeId = $eavConfig->getEntityType(MageProduct::ENTITY)->getId();
    }

    /**
     * Install product attributes and attribute sets
     *
     * @throws Zend_Validate_Exception
     */
    public function install(): void
    {
        try {
            foreach (array_keys($this->attributeSets) as $setName) {
                $this->installSet($setName);
                foreach (array_keys($this->attributes) as $attrName) {
                    if ($this->attributes[$attrName]['attribute_set_id'] == $setName) {
                        $this->installAttribute($attrName);
                    }
                }
            }
        } catch (InputException|NoSuchEntityException|LocalizedException $e) {
            $this->logger->error(
                sprintf('Cannot create catalog product configuration'),
                [$e]
            );
        }
    }

    /**
     * Install new attribute set
     *
     * @param string $setName The attribute set name
     *
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function installSet(string $setName)
    {
        $criteria = $this->criteriaBuilder
            ->addFilter(self::ATTR_SET_NAME_KEY, $setName, 'eq')
            ->addFilter(self::ENTITY_TYPE_ID_KEY, $this->productEntityTypeId, 'eq')
            ->create();
        if (count($this->setRepository->getList($criteria)->getItems()) > 0) {
            $this->logger->warning(
                sprintf('Attribute set <%s> is already created', $setName)
            );
        } else {
            $baseSetName = $this->attributeSets[$setName];
            $baseCriteria = $this->criteriaBuilder
                ->addFilter(self::ATTR_SET_NAME_KEY, $baseSetName, 'eq')
                ->addFilter(self::ENTITY_TYPE_ID_KEY, $this->productEntityTypeId, 'eq')
                ->create();
            $base = $this->setRepository->getList($baseCriteria)->getItems();
            $base = array_shift($base);
            if ($base != null) {
                $newSet = $this->setFactory->create();
                if ($newSet instanceof Set) {
                    $newSet->setData(
                        [
                            self::ATTR_SET_NAME_KEY => $setName,
                            self::ENTITY_TYPE_ID_KEY => $this->productEntityTypeId,
                            self::SORT_ORDER_KEY => 100
                        ]
                    );
                    $newSet->validate();
                    $this->setRepository->save($newSet);
                    $newSet->initFromSkeleton($base->getAttributeSetId());
                    $this->setRepository->save($newSet);
                } else {
                    $this->logger->error(
                        sprintf(
                            'Attribute set factory create %s but %s is expected',
                            get_class($newSet),
                            Set::class
                        )
                    );
                }
            } else {
                $this->logger->error(
                    sprintf('Cannot find base attribute set <%s> for set <%s>', $baseSetName, $setName)
                );
            }

        }
    }

    /**
     * Install product attribute
     *
     * @param string $attrName The attribute code
     *
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function installAttribute(string $attrName)
    {
        if ($this->eavConfig->getAttribute(MageProduct::ENTITY, $attrName)->getId() != null) {
            $this->logger->warning(
                sprintf('Product attribute ' . $attrName . ' is already created')
            );
        } else {
            $eavSetup = $this->eavSetupFactory->create();
            $eavSetup->addAttribute(
                MageProduct::ENTITY,
                $attrName,
                $this->attributes[$attrName]
            );
        }
    }

    /**
     * Uninstall product attributes and attribute sets
     */
    public function uninstall(): void
    {
        $eavSetup = $this->eavSetupFactory->create();
        foreach (array_keys($this->attributes) as $attrName) {
            $eavSetup->removeAttribute(MageProduct::ENTITY, $attrName);
        }
        foreach (array_keys($this->attributeSets) as $setName) {
            $criteria = $this->criteriaBuilder
                ->addFilter(self::ATTR_SET_NAME_KEY, $setName, 'eq')
                ->addFilter(self::ENTITY_TYPE_ID_KEY, $this->productEntityTypeId, 'eq')
                ->create();
            $set = array_shift($this->setRepository->getList($criteria)->getItems());
            if ($set != null) {
                try {
                    $this->setRepository->deleteById($set->getId());
                } catch (InputException|NoSuchEntityException $e) {
                    $this->logger->error('Cannot remove attribute set: ' . $setName);
                }
            }
        }
    }
}
