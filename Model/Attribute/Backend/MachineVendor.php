<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:34
 */

namespace OooAst\Catalog\Model\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend;

/**
 * Class MachineType
 *
 * @package OooAst\CatalogConfiguration\Model\Attribute\Backend
 */
class MachineVendor extends ArrayBackend
{
    /**
     * @inheritDoc
     */
    public function validate($object)
    {
        return parent::validate($object);
    }
}
