<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 26.05.2019
 * Time: 11:24
 */

namespace OooAst\Catalog\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;
use OooAst\Catalog\Model\Category;
use OooAst\Catalog\Model\Product;
use Psr\Log\LoggerInterface;

/**
 * Uninstall OOO Ast catalog configuration
 *
 * @package OooAst\CatalogConfiguration\Setup
 */
class Uninstall implements UninstallInterface
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Category
     */
    private $category;

    /**
     * Uninstall constructor.
     *
     * @param LoggerInterface $logger
     * @param Product $product
     * @param Category $category
     */
    public function __construct(
        LoggerInterface $logger,
        Product $product,
        Category $category
    ) {
        $this->logger = $logger;
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Invoked when remove-data flag is set during module uninstall.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void

     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->product->uninstall();
        $this->category->uninstall();
        $setup->endSetup();
    }
}
