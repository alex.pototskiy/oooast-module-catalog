<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:43
 */

namespace OooAst\Catalog\Model\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;

/**
 * Machine type frontend
 *
 * @package OooAst\Catalog\Model\Attribute\Frontend
 */
class MachineModel extends AbstractFrontend
{
    /**
     * @inheritDoc
     */
    public function getValue(DataObject $object)
    {
        $value = '';
        $partsString = $object->getData('machine_model');
        if ($partsString != null) {
            $parts = explode(',', $partsString);
            foreach ($parts as $part) {
                $value .= '<span class="machine_model">' . trim($part) . '</span>';
            }
        }
        return $value;
    }

}
