<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast Catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:25
 */

namespace OooAst\Catalog\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class MachineType
 *
 * @package OooAst\CatalogConfiguration\Model\Attribute\Source
 */
class MachineType extends AbstractSource
{
    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Bulldozer'), 'value' => 'bulldozer'],
                ['label' => __('Excavator'), 'value' => 'excavator'],
                ['label' => __('Grader'), 'value' => 'grader'],
                ['label' => __('Mini Excavator'), 'value' => 'mini-excavator'],
                ['label' => __('Not Defined'), 'value' => 'not-defined'],
            ];
        }
        return $this->_options;
    }
}
