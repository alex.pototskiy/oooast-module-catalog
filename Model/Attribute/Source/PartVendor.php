<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast Catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:25
 */

namespace OooAst\Catalog\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class MachineUnit
 *
 * @package OooAst\CatalogConfiguration\Model\Attribute\Source
 */
class PartVendor extends AbstractSource
{
    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Esco'), 'value' => 'esco'],
                ['label' => __('NOK'), 'value' => 'nok'],
                ['label' => __('Monbow'), 'value' => 'monbow'],
                ['label' => __('Hanwoo'), 'value' => 'hanwoo'],
                ['label' => __('Cummins'), 'value' => 'cummins'],
                ['label' => __('Perkins'), 'value' => 'perkins'],
                ['label' => __('Not Defined'), 'value' => 'not-defined'],
            ];
        }
        return $this->_options;
    }
}
