<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast Catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:25
 */

namespace OooAst\Catalog\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class MachineUnit
 *
 * @package OooAst\CatalogConfiguration\Model\Attribute\Source
 */
class MachineVendor extends AbstractSource
{
    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Caterpillar'), 'value' => 'caterpillar'],
                ['label' => __('Komatsu'), 'value' => 'komatsu'],
                ['label' => __('Volvo'), 'value' => 'volvo'],
                ['label' => __('Hitachi'), 'value' => 'hitachi'],
                ['label' => __('Doosan'), 'value' => 'doosan'],
                ['label' => __('Hyundai'), 'value' => 'hyundai'],
                ['label' => __('JCB'), 'value' => 'jcb'],
                ['label' => __('NHC'), 'value' => 'nhc'],
                ['label' => __('Liugong'), 'value' => 'liugong'],
                ['label' => __('XCMG'), 'value' => 'xcmg'],
                ['label' => __('XGMA'), 'value' => 'xgma'],
                ['label' => __('Lonking'), 'value' => 'lonking'],
                ['label' => __('Terex'), 'value' => 'terex'],
                ['label' => __('Mitsubishi'), 'value' => 'mitsubishi'],
                ['label' => __('Isuzu'), 'value' => 'isuzu'],
                ['label' => __('SEM'), 'value' => 'sem'],
                ['label' => __('Kobelco'), 'value' => 'kobelco'],
                ['label' => __('Liebherr'), 'value' => 'liebherr'],
                ['label' => __('Yanmar'), 'value' => 'yanmar'],
                ['label' => __('KATO Works'), 'value' => 'kato-works'],
                ['label' => __('SUMITOMO'), 'value' => 'sumitomo'],
                ['label' => __('Kubota'), 'value' => 'kubota'],
                ['label' => __('FURUKAWA'), 'value' => 'furukawa'],
                ['label' => __('TADANO'), 'value' => 'tadano'],
                ['label' => __('SHEHWA'), 'value' => 'shehwa'],
                ['label' => __('Shantui'), 'value' => 'shantui'],
                ['label' => __('Not Defined'), 'value' => 'not-defined'],
            ];
        }
        return $this->_options;
    }
}
