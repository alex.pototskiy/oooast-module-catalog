<?php
declare(strict_types=1);

namespace OooAst\Catalog\Plugin\Api;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryExtensionFactory;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Category;
use OooAst\Catalog\Api\Data\CategoryGroupCodeInterface;

/**
 * Plugin to handle load and save category to set OOO Ast extension attributes
 *
 * @package OooAst\Catalog\Plugin\Api
 */
class CategoryRepositoryInterfacePlugin
{
    /**
     * @var CategoryExtensionFactory
     */
    private $categoryExtensionFactory;

    /**
     * CategoryRepositoryInterfacePlugin constructor.
     *
     * @param CategoryExtensionFactory $extensionFactory
     */
    public function __construct(
        CategoryExtensionFactory $extensionFactory
    ) {
        $this->categoryExtensionFactory = $extensionFactory;
    }

    /**
     * Set category group code extension attribute
     *
     * @param CategoryRepositoryInterface $subject
     * @param CategoryInterface $result
     *
     * @return CategoryInterface
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */

    public function afterGet(
        /** @noinspection PhpUnusedParameterInspection */ CategoryRepositoryInterface $subject,
        CategoryInterface $result
    ): CategoryInterface {
        if ($result instanceof Category) {
            if ($result->getExtensionAttributes() == null) {
                $result->setExtensionAttributes(
                    $this->categoryExtensionFactory->create()
                );
            }
            if ($result->getId() == 1) {
                $result->getExtensionAttributes()->setSkg(CategoryGroupCodeInterface::ROOT_SKG);
            } else {
                $result->getExtensionAttributes()->setSkg($result->getData('skg'));
            }
        }
        return $result;
    }

    /**
     * Set data for group code attribute before save
     *
     * @param CategoryRepositoryInterface $subject
     * @param CategoryInterface $category
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function beforeSave(
        /** @noinspection PhpUnusedParameterInspection */ CategoryRepositoryInterface $subject,
        CategoryInterface $category
    ) {
        if ($category instanceof Category) {
            if ($category->getExtensionAttributes() &&
                $category->getExtensionAttributes()->getSkg()) {
                $category->setData('skg', $category->getExtensionAttributes()->getSkg());
            }
        }
        return [$category];
    }
}
