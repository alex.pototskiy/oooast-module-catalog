<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:43
 */

namespace OooAst\Catalog\Model\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Eav\Model\Entity\Attribute\Source\BooleanFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Serialize\Serializer\Json as Serializer;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Machine type frontend
 *
 * @package OooAst\Catalog\Model\Attribute\Frontend
 */
class MachineVendor extends AbstractFrontend
{
    /**
     * @var \OooAst\Catalog\Model\Attribute\Source\MachineVendor
     */
    private $machineVendor;

    /**
     * MachineVendor constructor.
     *
     * @param BooleanFactory $attrBooleanFactory
     * @param CacheInterface|null $cache
     * @param null $storeResolver
     * @param array|null $cacheTags
     * @param StoreManagerInterface|null $storeManager
     * @param Serializer|null $serializer
     * @param \OooAst\Catalog\Model\Attribute\Source\MachineVendor $machineVendor
     */
    public function __construct(
        BooleanFactory $attrBooleanFactory,
        \OooAst\Catalog\Model\Attribute\Source\MachineVendor $machineVendor,
        CacheInterface $cache = null,
        $storeResolver = null,
        array $cacheTags = null,
        StoreManagerInterface $storeManager = null,
        Serializer $serializer = null
    ) {
        parent::__construct($attrBooleanFactory, $cache, $storeResolver, $cacheTags, $storeManager, $serializer);
        $this->machineVendor = $machineVendor;
    }

    /**
     * @inheritDoc
     */
    public function getValue(DataObject $object)
    {
        $vendors = $object->getData('machine_vendor');
        $value = '<ul class="vendor-list">';
        if ($vendors != null) {
            $vendors = explode(',', $vendors);
            foreach ($vendors as $item) {
                $value .= '<li>' . $this->machineVendor->getOptionText($item) . '</li>';
            }
        }
        $value .= '</ul>';
        return $value;
    }

}
