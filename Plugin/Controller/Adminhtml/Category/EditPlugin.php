<?php
declare(strict_types=1);

namespace OooAst\Catalog\Plugin\Controller\Adminhtml\Category;

use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Controller\Adminhtml\Category\Edit;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class EditPlugin
{
    private $categoryRepository;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Edit $subject
     * @param ResultInterface $result
     *
     * @return ResultInterface
     * @throws NoSuchEntityException
     */
    public function afterExecute(Edit $subject, $result)
    {
        $categoryId = $subject->getRequest()->getParam('id');
        $category = $this->categoryRepository->get($categoryId);
        if (!$category) {
            return $result;
        }
        $skg = $category->getExtensionAttributes()->getSkg();
        if (!empty($skg) && $result instanceof Page) {
            $result->getConfig()->getTitle()->prepend(
                $category->getName() . ' (ID: ' . $categoryId . ', SKG: ' . $skg . ')'
            );
        }
        return $result;
    }
}
