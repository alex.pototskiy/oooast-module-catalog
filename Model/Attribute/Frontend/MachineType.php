<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.05.2019
 * Time: 8:43
 */

namespace OooAst\Catalog\Model\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;

/**
 * Machine type frontend
 *
 * @package OooAst\Catalog\Model\Attribute\Frontend
 */
class MachineType extends AbstractFrontend
{
    /**
     * @inheritDoc
     */
    public function getValue(DataObject $object)
    {
        return parent::getValue($object); // TODO: Change the autogenerated stub
    }

}
