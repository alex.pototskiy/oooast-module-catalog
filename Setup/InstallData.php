<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Catalog configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 26.05.2019
 * Time: 11:25
 */

namespace OooAst\Catalog\Setup;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use OooAst\Catalog\Model\Category;
use OooAst\Catalog\Model\Product;
use Psr\Log\LoggerInterface;
use Zend_Validate_Exception;

/**
 * Create OOO Ast catalog configuration
 *
 * @package OooAst\CatalogConfiguration\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var Category
     */
    private $category;

    /**
     * InstallData constructor.
     *
     * @param LoggerInterface $logger
     * @param Product $product
     * @param Category $category
     */
    public function __construct(
        LoggerInterface $logger,
        Product $product,
        Category $category
    ) {
        $this->logger = $logger;
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->product->install();
        $this->category->install();
        $setup->endSetup();
    }

}
