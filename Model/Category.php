<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * OOO Ast catalog category configuration
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 28.05.2019
 * Time: 13:14
 */

namespace OooAst\Catalog\Model;

use Magento\Catalog\Model\Category as MageCategory;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Zend_Validate_Exception;

/**
 * Class Category configuration
 *
 * @package OooAst\Catalog\Model
 */
class Category
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    private $attributes = [
        'skg' => [
            'type' => 'varchar',
            'label' => 'Group Code',
            'input' => 'input',
            'is_unique' => true,
            'required' => true,
            'sort_order' => 100,
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'used_in_forms' => ['adminhtml_category']
        ]
    ];
    /**
     * @var Config
     */
    private $eavConfig;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Category constructor.
     *
     * @param LoggerInterface $logger
     * @param Config $eavConfig
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        LoggerInterface $logger,
        Config $eavConfig,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->logger = $logger;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Install extended catalog category attributes
     *
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function install()
    {
        $eavSetup = $this->eavSetupFactory->create();
        foreach (array_keys($this->attributes) as $attrName) {
            if ($this->eavConfig->getAttribute(MageCategory::ENTITY, $attrName)->getId() != null) {
                $this->logger->warning('Category attribute ' . $attrName . ' is already created');
            } else {
                $eavSetup->addAttribute(MageCategory::ENTITY, $attrName, $this->attributes[$attrName]);
            }
        }
    }

    /**
     * Uninstall extended catalog category attributes
     */
    public function uninstall()
    {
        $eavSetup = $this->eavSetupFactory->create();
        foreach (array_keys($this->attributes) as $attrName) {
            $eavSetup->removeAttribute(
                MageCategory::ENTITY,
                $attrName
            );
        }
    }
}
