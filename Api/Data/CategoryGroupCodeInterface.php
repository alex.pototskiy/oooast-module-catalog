<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Category group code interface
 *
 * @category Catalog
 * @package  OooAst_Catalog
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 17.04.2019
 * Time: 20:11
 */

namespace OooAst\Catalog\Api\Data;

interface CategoryGroupCodeInterface
{
    /**
     * Group code for category with id(0) that is not really in DB
     */
    const ROOT_PARENT_SKG = 'X.000.000.000.0000';
    /**
     * Group code for category with id(1) that is root for all other categories
     */
    const ROOT_SKG = 'X.000.000.000.0001';
}
